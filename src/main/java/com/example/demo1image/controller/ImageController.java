package com.example.demo1image.controller;

import com.example.demo1image.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.springframework.util.MimeTypeUtils.IMAGE_PNG_VALUE;

@RestController
@RequestMapping("test")
public class ImageController {

    @Autowired
    private ImageService service;

    @PostMapping()
    public ResponseEntity<?> creatImage(@RequestParam("image") MultipartFile file) throws IOException {
        String image = service.createImage(file);
        return ResponseEntity.status(HttpStatus.OK)
                .body(image);
    }

    @GetMapping("/{fileName}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable String fileName) {
        byte[] imageData = service.downloadImage(fileName);
        // Assurez-vous d'ajuster MediaType.IMAGE_JPEG selon le type de fichier que vous servez
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG); // Remplacez MediaType.IMAGE_JPEG par le type de média approprié

        return ResponseEntity.ok()
                .headers(headers)
                .body(imageData);
    }

    @GetMapping("/t/{id}")
    public ResponseEntity<byte[]> getImageById(@PathVariable int id){
        byte[] imageData = service.getImageById(id);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        return ResponseEntity.ok()
                .headers(headers)
                .body(imageData);
    }
}
