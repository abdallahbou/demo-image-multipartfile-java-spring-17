package com.example.demo1image.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    String createImage(MultipartFile file) throws IOException;

    byte[] downloadImage(String imageName);

    byte[]getImageById(int id);





}
