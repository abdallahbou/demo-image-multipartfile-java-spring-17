package com.example.demo1image.service;

import com.example.demo1image.entity.Image;
import com.example.demo1image.repository.ImageRepository;
import com.example.demo1image.util.ImageUtils;
import org.apache.commons.lang3.exception.ContextedRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;

@Service
public class ImageServiceImpl implements ImageService{

    @Autowired
    private ImageRepository repository;
    @Override
    public String createImage(MultipartFile file) throws IOException {

        Image image = repository.save(Image.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .imageData(ImageUtils.compressImage(file.getBytes())).build());
        if (image != null){
            return "file upload succefully : "+file.getOriginalFilename();
        }
        return null;
    }



    public byte[] downloadImage(String imageName) {
        Optional<Image> dbImage = repository.findByName(imageName);
        return dbImage.map(image -> {
            return ImageUtils.decompressImage(image.getImageData());
        }).orElse(null);
    }

    @Override
    public byte[] getImageById(int id) {
        Optional<Image> dbImage= repository.findById(id);

        return dbImage.map(image -> {
            try {
                return ImageUtils.decompressData1(image.getImageData());
            } catch (DataFormatException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).orElse(null);
    }


}
