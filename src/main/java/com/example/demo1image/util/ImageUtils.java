package com.example.demo1image.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ImageUtils {



    // fonction pour compresser le fichier (l'image dans cette situation)
public static byte[] compressImage(byte[] data) {
    Deflater deflater = new Deflater();
    deflater.setLevel(Deflater.BEST_COMPRESSION);
    deflater.setInput(data);
    deflater.finish();

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
    byte[] tmp = new byte[4 * 1024];

    try {
        while (!deflater.finished()) {
            int size = deflater.deflate(tmp);
            outputStream.write(tmp, 0, size);
        }

        return outputStream.toByteArray();

    } finally {
        // Assurez-vous de fermer le flux ByteArrayOutputStream dans le bloc finally pour garantir sa fermeture
        try {
            outputStream.close();
        } catch (Exception ignored) {
            // Gestion de l'exception
        }
        deflater.end(); // Libérer les ressources de l'objet Deflater
    }
}

 // fonction pour décompresser l'image et pouvoir le voir
    public static byte[] decompressImage(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] tmp = new byte[4*1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(tmp);
                outputStream.write(tmp, 0, count);
            }
            outputStream.close();
        } catch (Exception ignored) {
        }
        return outputStream.toByteArray();
    }

// fonction avec exception
    public static byte[] decompressImage1(byte[] data) throws DataFormatException, IOException {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] tmp = new byte[4*1024];

        while (!inflater.finished()) {
            int count = inflater.inflate(tmp);
            outputStream.write(tmp, 0, count);
        }

        outputStream.close();
        return outputStream.toByteArray();
    }


// fonction avec les log pour tester la fonction et voir les beug
    public static byte[] decompressData1(byte[] data) throws DataFormatException, IOException {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] tmp = new byte[4 * 1024];

        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(tmp);
                outputStream.write(tmp, 0, count);

                // Ajout de logs pour tester
                System.out.println("Bytes read: " + count);
                System.out.println("Total Bytes Read So Far: " + inflater.getBytesRead());
                System.out.println("Total Bytes Written So Far: " + inflater.getBytesWritten());
            }

            return outputStream.toByteArray();

        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            inflater.end(); // Libérer les ressources de l'objet Inflater
        }
    }
}
