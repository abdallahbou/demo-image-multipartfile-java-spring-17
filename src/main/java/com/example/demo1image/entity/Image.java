package com.example.demo1image.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ImageData")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String type;

    // permet de stocker l'identitée numérique de l'image et LONGBLOB permet d'ajuster la taille de la colomne
    @Lob
    @Column(name = "imagedata", length = 1000, columnDefinition = "LONGBLOB")
    private byte[] imageData;



}
