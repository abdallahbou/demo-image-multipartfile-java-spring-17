package com.example.demo1image;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1ImageApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo1ImageApplication.class, args);
    }

}
