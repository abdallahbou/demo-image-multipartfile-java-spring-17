package com.example.demo1image.serviceTest;

import com.example.demo1image.entity.Image;
import com.example.demo1image.repository.ImageRepository;
import com.example.demo1image.service.ImageService;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ImageServiceImplTest {


//    @Test
//    public void testDownloadImage() {
//        ImageRepository repository = mock(ImageRepository.class);
//        ImageService imageService;
//        imageService = new ImageService() {
//            @Override
//            public String createImage(MultipartFile file) throws IOException {
//                return null;
//            }
//
//            @Override
//            public byte[] getImageByName(String name) {
//                return new byte[0];
//            }
//
//            @Override
//            public byte[] downloadImage(String imageName) {
//                return new byte[0];
//            }
//        };
//
//        // Données d'image compressée simulées
//        byte[] compressedImageData = new byte[]{
//                80, 75, 3, 4, 20, 0, 8, 8, 0, 0, 23, 90, 86, 15, 20, 104, 1, 0, 0, 3, 1, 0, 0, 49, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 32, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 97, 98, 99, 46, 106, 112, 103, // ...
//        };
//
//        Image image = new Image();
//        image.setImageData(compressedImageData);
//
//        when(repository.findByName(anyString())).thenReturn(Optional.of(image));
//
//        // Ajustement de la méthode downloadImage simulée pour qu'elle retourne les données d'image compressée simulées
//        ImageService imageServiceSpy = spy(imageService);
//        doReturn(compressedImageData).when(imageServiceSpy).downloadImage(anyString());
//
//        byte[] actualDecompressedData = imageServiceSpy.downloadImage("example.jpg");
//
//        assertNotNull(actualDecompressedData);
//        assertArrayEquals(compressedImageData, actualDecompressedData);
//    }

}
